<?php

/**
 * @file
 * Post update functions for the Social Auth HID module.
 */

/**
 * Adds HID base URL setting.
 */
function social_auth_hid_add_url_setting() {
  $config = \Drupal::configFactory()
    ->getEditable('social_auth_hid.settings');

  $config->set('base_url', 'https://auth.humanitarian.id')
    ->save(TRUE);
}

/**
 * Set scopes and endpoints settings.
 */
function social_auth_hid_add_scopes_and_endpoints_setting() {
  $config = \Drupal::configFactory()
    ->getEditable('social_auth_hid.settings');

  $scopes = $config->get('scopes');
  $endpoints = $config->get('endpoints');

  if (is_null($scopes) || is_null($endpoints)) {
    $config
      ->set('scopes', $scopes ?? '')
      ->set('endpoints', $endpoints ?? '')
      ->save(TRUE);
  }
}
