<?php

namespace Drupal\Tests\social_auth_hid\Functional;

use Drupal\Tests\social_auth\Functional\SocialAuthTestBase;

/**
 * Test Social Auth Humanitarian ID settings form.
 *
 * @group social_auth
 *
 * @ingroup social_auth_hid
 */
class SocialAuthHidSettingsFormTest extends SocialAuthTestBase {
  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['social_auth_hid'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    $this->module = 'social_auth_hid';
    $this->moduleType = 'social-auth';
    $this->provider = 'hid';

    parent::setUp();
  }

  /**
   * {@inheritdoc}
   *
   * @test
   * @doesNotPerformAssertions
   */
  public function checkIsAvailableInIntegrationList() {
    $this->fields = ['client_id', 'client_secret'];

    parent::checkIsAvailableInIntegrationList();
  }

  /**
   * {@inheritdoc}
   *
   * @test
   * @doesNotPerformAssertions
   */
  public function checkSettingsFormSubmission() {
    $this->edit = [
      'client_id' => $this->randomString(10),
      'client_secret' => $this->randomString(10),
    ];

    parent::checkSettingsFormSubmission();
  }

}
