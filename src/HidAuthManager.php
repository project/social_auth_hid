<?php

namespace Drupal\social_auth_hid;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\social_auth\AuthManager\OAuth2Manager;
use Drupal\social_auth\User\SocialAuthUser;
use Drupal\social_auth\User\SocialAuthUserInterface;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Contains all the logic for Humanitarian ID OAuth2 authentication.
 */
class HidAuthManager extends OAuth2Manager {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Used for accessing configuration object factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The current request.
   */
  public function __construct(ConfigFactory $config_factory, LoggerChannelFactoryInterface $logger_factory, RequestStack $request_stack) {
    parent::__construct($config_factory->get('social_auth_hid.settings'), $logger_factory, $request_stack->getCurrentRequest());
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate() {
    try {
      $this->setAccessToken($this->client->getAccessToken('authorization_code',
        ['code' => $this->request->query->get('code')]));
    }
    catch (IdentityProviderException $exception) {
      $this->loggerFactory->get('social_auth_hid')
        ->error('There was an error during authentication. Exception: ' . $exception->getMessage());
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getUserInfo(): ?SocialAuthUserInterface {
    if (!$this->user) {
      /** @var \League\OAuth2\Client\Provider\GoogleUser $owner */
      $owner = $this->client->getResourceOwner($this->getAccessToken());
      $this->user = new SocialAuthUser(
        $owner->getName(),
        $owner->getId(),
        $this->getAccessToken(),
        $owner->getEmail(),
        $owner->getAvatar(),
        $this->getExtraDetails()
      );
      $this->user->setFirstName($owner->getFirstName());
      $this->user->setLastName($owner->getLastName());
    }
    return $this->user;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthorizationUrl(): mixed {
    $scopes = [
      'profile',
    ];

    $hid_scopes = $this->getScopes();
    if ($hid_scopes) {
      if (strpos($hid_scopes, ',')) {
        $scopes = array_merge($scopes, explode(',', $hid_scopes));
      }
      else {
        $scopes[] = $hid_scopes;
      }
    }

    // Returns the URL where user will be redirected.
    return $this->client->getAuthorizationUrl([
      'scope' => $scopes,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function requestEndPoint(string $method, string $path, ?string $domain = NULL, array $options = []): mixed {
    if (!$domain) {
      $domain = $this->settings->get('base_url');
    }

    $url = $domain . $path;

    $request = $this->client->getAuthenticatedRequest($method, $url, $this->getAccessToken(), $options);

    try {
      return $this->client->getParsedResponse($request);
    }
    catch (IdentityProviderException $exception) {
      $this->loggerFactory->get('social_auth_google')
        ->error('There was an error when requesting ' . $url . '. Exception: ' . $exception->getMessage());
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getState(): string {
    return $this->client->getState();
  }

}
