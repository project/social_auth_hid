<?php

namespace Drupal\social_auth_hid\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\Form\SocialAuthSettingsForm;
use Drupal\social_auth\Plugin\Network\NetworkInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for Social Auth Humanitarian ID.
 */
final class HidAuthSettingsForm extends SocialAuthSettingsForm {

  /**
   * Route builder.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected RouteBuilderInterface $routeBuilder;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\social_api\Plugin\NetworkManager $network_manager
   *   Network manager.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   Used to check if route exists.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   Route builder.
   */
  public function __construct(ConfigFactoryInterface $config_factory, NetworkManager $network_manager, RouteProviderInterface $route_provider, RouteBuilderInterface $route_builder) {
    parent::__construct($config_factory, $network_manager, $route_provider);
    $this->routeBuilder = $route_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.network.manager'),
      $container->get('router.route_provider'),
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'social_auth_hid_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return array_merge(
      parent::getEditableConfigNames(),
      ['social_auth_hid.settings']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?NetworkInterface $network = NULL): array {
    $network = $network ?? $this->networkManager->createInstance('social_auth_hid');

    $form = parent::buildForm($form, $form_state, $network);

    $config = $this->config('social_auth_hid.settings');

    $form['network']['#description'] = $this->t('You need to first request the creation of an HID OAuth application by emailing <a href="mailto:@hid-info">@hid-info</a>', ['@hid-info' => 'info@humanitarian.id']);

    $form['network']['advanced']['base_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('HID base URL'),
      '#description' => $this->t('Customize your HID base URL, e.g. if you want to use HID dev or HID staging.'),
      '#default_value' => $config->get('base_url'),
    ];

    $form['network']['advanced']['auto_redirect'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto redirect to HID'),
      '#default_value' => $config->get('auto_redirect'),
    ];

    $form['network']['advanced']['disable_default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable default user login and reset forms'),
      '#default_value' => $config->get('disable_default'),
    ];

    $form['network']['advanced']['disable_password_fields'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide password fields on user register and edit pages'),
      '#default_value' => $config->get('disable_password_fields'),
    ];

    $form['network']['advanced']['disable_email_field'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable email field on user edit page'),
      '#default_value' => $config->get('disable_email_field'),
    ];

    $form['network']['advanced']['disable_user_field'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable name field on user edit page'),
      '#default_value' => $config->get('disable_user_field'),
    ];

    $form['network']['advanced']['disable_user_create'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable user create form'),
      '#description' => $this->t('Ensure users have a social_auth login by disallowing creation of users directly in the ѕite.'),
      '#default_value' => $config->get('disable_user_create'),
    ];

    $form['network']['advanced']['maintenance_access'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow HID logins during maintenance mode'),
      '#default_value' => $config->get('maintenance_access'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    parent::validateForm($form, $form_state);
    if (!UrlHelper::isValid($values['base_url'], TRUE)) {
      $form_state->setErrorByName('base_url', $this->t("The HID base URL is invalid."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $this->config('social_auth_hid.settings')
      ->set('client_id', trim($values['client_id']))
      ->set('client_secret', trim($values['client_secret']))
      ->set('base_url', rtrim($values['base_url'], '/'))
      ->set('auto_redirect', $values['auto_redirect'])
      ->set('disable_default', $values['disable_default'])
      ->set('disable_password_fields', $values['disable_password_fields'])
      ->set('disable_email_field', $values['disable_email_field'])
      ->set('disable_user_field', $values['disable_user_field'])
      ->set('disable_user_create', $values['disable_user_create'])
      ->set('maintenance_access', $values['maintenance_access'])
      ->save();

    // Clear router cache.
    $this->routeBuilder->rebuild();

    parent::submitForm($form, $form_state);
  }

}
