<?php

namespace Drupal\social_auth_hid\Settings;

use Drupal\social_auth\Settings\SettingsInterface;

/**
 * Defines an interface for Social Auth Hid settings.
 */
interface HidAuthSettingsInterface extends SettingsInterface {

  /**
   * Gets the base URL.
   *
   * @return string
   *   The HID base URL.
   */
  public function getBaseUrl(): ?string;

}
