<?php

namespace Drupal\social_auth_hid\Settings;

use Drupal\social_auth\Settings\SettingsBase;

/**
 * Defines methods to get Social Auth Hid settings.
 */
class HidAuthSettings extends SettingsBase implements HidAuthSettingsInterface {

  /**
   * The base URL.
   *
   * @var string|null
   */
  protected ?string $baseUrl = NULL;

  /**
   * {@inheritdoc}
   */
  public function getBaseUrl(): ?string {
    if (!$this->baseUrl) {
      $this->baseUrl = $this->config->get('base_url');
    }
    return $this->baseUrl;
  }

}
