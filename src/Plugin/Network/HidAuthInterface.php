<?php

namespace Drupal\social_auth_hid\Plugin\Network;

use Drupal\social_auth\Plugin\Network\NetworkInterface;

/**
 * Defines the Hid Auth interface.
 */
interface HidAuthInterface extends NetworkInterface {}
