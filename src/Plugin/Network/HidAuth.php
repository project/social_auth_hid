<?php

namespace Drupal\social_auth_hid\Plugin\Network;

use Drupal\Core\Url;
use Drupal\social_auth\Plugin\Network\NetworkBase;

/**
 * Defines a Network Plugin for Social Auth Humanitarian ID.
 *
 * @package Drupal\social_auth_hid\Plugin\Network
 *
 * @Network(
 *   id = "social_auth_hid",
 *   short_name = "HID",
 *   social_network = "Humanitarian ID",
 *   img_path = "img/hid_logo.png",
 *   type = "social_auth",
 *   class_name = "\League\OAuth2\Client\Provider\HumanitarianId",
 *   auth_manager = "\Drupal\social_auth_hid\HidAuthManager",
 *   routes = {
 *     "redirect": "social_auth_hid.redirect_to_hid",
 *     "callback": "social_auth_hid.callback",
 *     "settings_form": "social_auth_hid.settings_form",
 *   },
 *   handlers = {
 *     "settings": {
 *       "class": "\Drupal\social_auth_hid\Settings\HidAuthSettings",
 *       "config_id": "social_auth_hid.settings"
 *     }
 *   }
 * )
 */
class HidAuth extends NetworkBase implements HidAuthInterface {

  /**
   * {@inheritdoc}
   */
  protected function getExtraSdkSettings(): array {
    /** @var \Drupal\social_auth_hid\Settings\HidAuthSettings $settings */
    $settings = $this->settings;
    return ['domain' => $settings->getBaseUrl()];
  }

  /**
   * {@inheritdoc}
   */
  public function getCallbackUrl(array $route_options = []): Url {
    return $this->getUrlFromDefaultRoute('callback', $route_options);
  }

  /**
   * {@inheritdoc}
   */
  private function getUrlFromDefaultRoute(string $type, array $route_options = []): Url {
    return Url::fromRoute(
      $this->getPluginDefinition()['routes'][$type],
      // Do not pass the network parameter as it's unneeded with our route
      // handlers and breaks backward compatibility.
      // @see social_auth_hid.routing.yml
      [],
      $route_options
    );
  }

}
