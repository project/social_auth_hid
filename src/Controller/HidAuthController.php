<?php

namespace Drupal\social_auth_hid\Controller;

use Drupal\social_auth\Controller\OAuth2ControllerBase;
use Drupal\social_auth\Plugin\Network\NetworkInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for Social Auth Hid module routes.
 */
class HidAuthController extends OAuth2ControllerBase {

  /**
   * {@inheritdoc}
   *
   * Response for path 'user/login/hid/callback'.
   *
   * Prevent redirection loop.
   */
  public function callback(?NetworkInterface $network = NULL): RedirectResponse {
    $network = $network ?? $this->networkManager->createInstance('social_auth_hid');

    // Checks if there was an authentication error.
    $response = $this->checkAuthError();

    // If there are no errors, attempt to authenticate the user.
    $response = $response ?? parent::callback($network);

    // Prevent redirect loop.
    //
    // This happens when `auto_redirect` is checked and a redirection to the
    // user login form is returned by `OAuth2ControllerBase::checkAuthError()`
    // or `userAuthenticator::authenticateUser()`.
    //
    // List of cases where a redirection to the login form occurs:
    //
    // - There is an authentication error on the provider side.
    //
    //   See OAuth2ControllerBase::checkAuthError().
    //   See UserAuthenticator::dispatchAuthenticationError().
    //
    // - The provider could not be associated with the account (error).
    //
    //   See UserAuthenticator::associateNewProvider().
    //
    // - Trying to log in with the user 1 when authenticating with the admin
    //   account is disabled.
    //
    //   See UserAuthenticator::authenticateExistingUser().
    //
    // - Trying to log in with an account that has a role for which logging in
    //   with Social Auth is disabled.
    //
    //   See UserAuthenticator::authenticateExistingUser().
    //
    // - Trying to log in with an account that is blocked (not yet approved
    //   for example).
    //
    //   See UserAuthenticator::authenticateExistingUser().
    //
    // - Logging in as a new user and admin approval is required.
    //
    //   See UserAuthenticator::authenticateNewUser().
    //
    // - Logging in as a new user and registration is disabled.
    //
    //   See UserAuthenticator::authenticateNewUser().
    //
    // @see \Drupal\social_auth\Controller\OAuth2ControllerBase::checkAuthError()
    // @see \Drupal\social_auth\User\UserAuthenticator
    // @see \Drupal\social_auth_hid\Routing\RouteSubscriber::alterRoutes()
    if (!$response || $response->getTargetUrl() === '/user/login/hid') {
      $response = $this->redirect('<front>');
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   *
   * @see https://www.drupal.org/project/social_auth/issues/3033444
   */
  public function redirectToProvider(?NetworkInterface $network = NULL): Response {
    $network = $network ?? $this->networkManager->createInstance('social_auth_hid');

    return parent::redirectToProvider($network);
  }

}
